/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "args.hpp"
#include "pcap.hpp"
#include <cctype>
#include <iostream>
#include <optional>
#include "log.hpp"
#include "frame.hpp"
#include <bit>
#include <signal.h>

void print_opt(const char *inter, const char *desc)
{
	printf("    %-45s %s\n", inter, desc);
}

void print_help(char *name)
{
	printf("Usage %s [OPTIONS]\n\n", name);
	printf("Where possible options are:\n");
	print_opt("-i --interface INTERFACE", "Interface on which to capture");
	print_opt("-i --interface", "Print list of interfaces");
	print_opt("-t --tcp", "Print all tcp packets");
	print_opt("-u --udp", "Print all udp packets");
	print_opt("-p PORT", "Filter TCP and UDP based on port");
	print_opt("--icmp4 --icmp6 --arp --ndp --igmp --mld",
		  "Only print specific protocols");
	print_opt("-n NUML", "Number of packets to print");
	print_opt("-h --help", "Print this message");
}

void check_endian()
{
	if constexpr (std::endian::native == std::endian::little)
		return;
	ERR("It apears this machine is not little endian.\n");
	ERR("While this program will attmpt to use big endian I could not test this actualy works.\n");
	ERR("So no guarantees.");
}

int main(int argc, char *argv[])
{
	check_endian();
	args arg;
	try {
		arg.parse(argc, argv);
	} catch (const arg_exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	if (arg.help) {
		print_help(argv[0]);
		return 0;
	}

	auto pcap_int = pcap(arg);
	if (arg.interface && arg.interface_name != NULL) {
		//initialize pcap
		if (arg.interface_name) {
			if (!pcap_int.select(arg.interface_name)) {
				ERR("Device %s not found", arg.interface_name);
				return 1;
			}
		}
	} else { //print all interfaces
		pcap_int.print();
		return 1;
	}

	if (!pcap_int.init()) {
		return 1;
	}

	pcap_int.add_callback(build_packaet);
	signal(SIGINT, [](int num){pcap_breakloop(pcap_handle);});
	pcap_int.start();

	return 0;
}
