/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef IGMP_H
#define IGMP_H
#include "../frame.hpp"
#include "../types.hpp"

namespace ipv4
{
//https://www.rfc-editor.org/rfc/rfc3376
//https://www.rfc-editor.org/rfc/rfc2236
struct igmp_packet : public ::packet {
public:
#pragma pack(push, 1)
	struct igmpv3_record_t {
		uint8_t type;
		uint8_t aux_data_len;
		uint16_t num_sources;
		address_t *addresses;
	};
	struct {
		uint8_t type;
		union {
			struct {
				uint8_t reserved;
				uint16_t checksum;
				address_t group_address;
			} v1;

			struct {
				uint8_t max_response_time;
				uint16_t reserved;
				address_t group_address;
			} v2;

			struct {
				uint8_t reserved1;
				uint16_t checksum;
				uint16_t reserved;
				uint16_t num_records;
				igmpv3_record_t *records;
			} v3;
		};
	} data;
#pragma pack(pop)

public:
		/** @see packet::packet */
	igmp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		    size_t used);
	virtual void print() override;
	virtual bool should_print(class args &args) override;
};

}

#endif /* IGMP_H */
