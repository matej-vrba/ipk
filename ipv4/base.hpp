/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef IPV4_BASE_H
#define IPV4_BASE_H
#include "../frame.hpp"
#include "../types.hpp"

namespace ipv4
{
enum class protocol {
	ICMP = 1,
	IGMP = 2,
	TCP = 6,
	UDP = 17,
};
// https://www.rfc-editor.org/rfc/rfc791
struct ipv4packet : public ::packet {
public:

	struct {
		#pragma pack(push, 1)
		uint8_t version_IHL;
		uint8_t service_type;
		uint16_t len;
		uint16_t id;
		uint16_t flags_frag_offset;
		uint8_t ttl;
		uint8_t protocol;
		uint16_t header_checksum;
		address_t src_add;
		address_t dst_add;
		#pragma pack(pop)
	} data;
	uint8_t *options = NULL;
	uint16_t options_len = 0;



public:
		/** @see packet::packet */
	ipv4packet(const struct pcap_pkthdr *header, const u_char *packet,
		   size_t used);
	virtual void print() override;
	virtual bool should_print(class args &args) override;
	virtual ~ipv4packet();

private:
	packet *next = NULL;
};

}

#endif /* IPV4_BASE_H */
