/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ICMPV4_H
#define ICMPV4_H
#include "../frame.hpp"

namespace ipv4
{
struct icmp_packet : public ::packet {
public:

	struct {
		#pragma pack(push, 1)
		uint8_t type;
		uint8_t code;
		uint16_t checksum;
		#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	icmp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		    size_t used);
	virtual void print() override;
	virtual bool should_print(class args &args) override;
};

}

#endif /* ICMPV4_H */
