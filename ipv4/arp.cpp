/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "arp.hpp"
#include "../args.hpp"
#include "../frame.hpp"
#include <cstring>

namespace ipv4
{
arp_packet::arp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		       size_t used)
{
	memcpy(&data, packet + used, sizeof(arp_packet::data));
	data.protocol_type = ntohs(data.protocol_type);
	data.hw_size = ntohs(data.hw_size);
	data.opcode = ntohs(data.opcode);
}

void arp_packet::print()
{
	printf("> %sARP%s\n", ORANGE, RESET);
	if(data.opcode == 1){
		printf("Opcode: request(1)\n");
	}else if(data.opcode == 2){
		printf("Opcode: response(2)\n");
	}else{
	DATA_PRINT(opcode);
	}
	print_ipv4("sender ip: ", data.sender_ip);
	print_mac("sender mac: ", data.sender_mac);
	print_ipv4("dst ip: ", data.target_ip);
	print_mac("dst mac: ", data.target_mac);
}

	bool arp_packet::should_print(class args &args){
		return args.print_all || args.arp;
	}

}
