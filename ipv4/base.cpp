/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../frame.hpp"
#include "icmp.hpp"
#include "igmp.hpp"
#include "base.hpp"
#include <assert.h>
#include "../L4/L4.hpp"
#include "../args.hpp"
#include <cstdint>
#include <cstring>
#include <stdlib.h>

namespace ipv4
{
ipv4packet::ipv4packet(const struct pcap_pkthdr *header, const u_char *packet,
		       size_t used)
{
	memcpy(&data, packet + used, sizeof(data));
	data.len = ntohs(data.len);
	data.id = ntohs(data.id);
	data.flags_frag_offset = ntohs(data.flags_frag_offset);
	data.header_checksum = ntohs(data.header_checksum);



	// length of whole ipv4 header including optional fields
	// header_len specifies number of 32-bit words -> * 4 to get number of bytes
	uint8_t header_len = (data.version_IHL & 0x0f) * 4;

	switch (data.protocol) {
	case (int)protocol::ICMP: {
		next = new icmp_packet(header, packet, used + header_len);
		return;
	}
	case (int)protocol::IGMP: {
		next = new igmp_packet(header, packet, used + header_len);
		return;
	}
	case (int)protocol::TCP: {
		next = new l4::tcp_packet(header, packet, used + header_len);
		return;
	}
	case (int)protocol::UDP: {
		next = new l4::udp_packet(header, packet, used + header_len);
		return;
	}
	}
	return;
}

ipv4packet::~ipv4packet()
{
	if (next)
		delete next;
}
void ipv4packet::print()
{
	printf("> %sIPv4 Header%s\n", ORANGE, RESET);
	print_ipv4("src_add: ", data.src_add);
	print_ipv4("dst_add: ", data.dst_add);
	DATA_PRINT(ttl);
	if (next)
		next->print();
}

bool ipv4packet::should_print(class args &args)
{
	if(next)
		return next->should_print(args);
	return args.print_all;

}
}
