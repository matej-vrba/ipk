/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ARP_H
#define ARP_H
#include "../frame.hpp"

namespace ipv4
{
struct arp_packet : public ::packet {
public:

	struct {
#pragma pack(push, 1)
		uint16_t hw_type;
		uint16_t protocol_type;
		uint8_t hw_size;
		uint8_t protocol_size;
		uint16_t opcode;
		mac_t sender_mac;
		address_t sender_ip;
		mac_t target_mac;
		address_t target_ip;
#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	arp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		   size_t used);
	virtual void print() override;
	virtual bool should_print(class args &args)override;
};
}

#endif /* ARP_H */
