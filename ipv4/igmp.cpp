/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "igmp.hpp"
#include "../frame.hpp"
#include "../args.hpp"
#include "../types.hpp"
#include <cstdint>
#include <cstring>

#define IGMP_QUERY 0x11
#define IGMPv1_REPORT 0x12
#define IGMPv2_REPORT 0x16
#define IGMPv2_LEAVE 0x17
#define IGMPv3_QUERY 0x22

namespace ipv4
{
igmp_packet::igmp_packet(const struct pcap_pkthdr *header, const u_char *packet,
			 size_t used)
{
	memcpy(&data, packet + used, sizeof(data.type));
	const uint8_t *next_record = NULL;
	size_t size;

	// memcpy is using + 1 to skip type
	switch (data.type) {
	case IGMP_QUERY:
	case IGMPv1_REPORT:
		memcpy(&data.v1, packet + used + 1, sizeof(data.v1));
		data.v1.checksum = htons(data.v1.checksum);
		break;
	case IGMPv2_REPORT:
	case IGMPv2_LEAVE:
		memcpy(&data.v2, packet + used + 1, sizeof(data.v2));
		break;
	case IGMPv3_QUERY:
		//read the packet without records
		size = sizeof(data.v3) - sizeof(void*);
		next_record = packet + used + 1;
		memcpy(&data.v3, next_record, size);
		next_record += size;

		data.v3.checksum = htons(data.v3.checksum);
		data.v3.num_records = htons(data.v3.num_records);
		// allocate memory for all the recordds
		data.v3.records = (igmpv3_record_t *)malloc(
			data.v3.num_records * sizeof(igmpv3_record_t));

		//load records individually
		for (int i = 0; i < data.v3.num_records; i++) {
			size = sizeof(igmpv3_record_t) - sizeof(void *);
			memcpy(&(data.v3.records[i]), next_record, size);
			next_record += size;

			data.v3.records[i].num_sources = ntohs(data.v3.records[i].num_sources);
			data.v3.records[i].addresses = (address_t*)malloc((data.v3.records[i].num_sources + 1) * sizeof(address_t));

			int num_addresses = data.v3.records[i].num_sources + 1;
			size = sizeof(address_t) * num_addresses;
			memcpy(data.v3.records[i].addresses, next_record, size);
			next_record += size;
			next_record += data.v3.records[i].aux_data_len;
		}
		break;
	default:
		LOG("Unknown IGMP type %d", data.type);
		break;
	}
}

void igmp_packet::print()
{
	printf("> %sIGMP%s\n", BLUE, RESET);
	switch (data.type) {
	case IGMP_QUERY:
		printf("type: v1 query\n");
		break;
	case IGMPv1_REPORT:
		printf("type: v1 report\n");
		break;
	case IGMPv2_REPORT:
		printf("type: v2 report\n");
		break;
	case IGMPv2_LEAVE:
		printf("type: leave\n");
		break;
	case IGMPv3_QUERY:
		printf("type: v3 query\n");
		break;
	default:
		DATA_PRINT(type);
	}

	switch (data.type) {
	case IGMP_QUERY:
	case IGMPv1_REPORT:
		print_ipv4("group address: ", data.v1.group_address);
		break;
	case IGMPv2_REPORT:
	case IGMPv2_LEAVE:
		print_ipv4("group address: ", data.v2.group_address);
		break;
	case IGMPv3_QUERY:
		printf("number of records: %d\n", data.v3.num_records);
		printf("checksum: %x\n", data.v3.checksum);
		for (int i = 0; i < data.v3.num_records; i++) {
			printf("> %srecord %s%d\n",GREEN, RESET, i);
			printf("type: %d\n", data.v3.records[i].type);
			printf("aux_data_len: %d\n", data.v3.records[i].aux_data_len);
			printf("num_sources: %d\n", data.v3.records[i].num_sources);
			print_ipv4("multicast addr: ", data.v3.records[i].addresses[0]);
			for (int j = 0; j < data.v3.records[i].num_sources; j++) {
				print_ipv4("source addr: ", data.v3.records[i].addresses[j + 1]);
			}
		}

		break;
	}
}

bool igmp_packet::should_print(class args &args)
{
	return args.print_all || args.igmp;
}

}
