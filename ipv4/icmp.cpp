/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "icmp.hpp"
#include "../args.hpp"
#include <cstring>

namespace ipv4
{
icmp_packet::icmp_packet(const struct pcap_pkthdr *header, const u_char *packet,
			 size_t used)
{
	memcpy(&data, packet + used, sizeof(icmp_packet::data));
	data.checksum = ntohs(data.checksum);
}

void icmp_packet::print()
{
	printf("> %sICMP%s\n", BLUE, RESET);
	if(data.type == 8){
		printf("type: echo request(8)\n");
	}else if (data.type == 0){
		printf("type: echo reply(0)\n");
	}else{
	printf("type: %x\n", data.type);
	}
	printf("code: %x\n", data.code);
}

bool icmp_packet::should_print(class args &args)
{
	return args.print_all || args.icmp4;
}

}
