/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ARGS_H_
#define ARGS_H_
#include <exception>
#include <string>

/**
 * @brief used to signalize invalid flag in program's `argv`
 *
 * All methods with arguments named `argv` and `argc` expect `argv` and `argc`
 * that main received
 */
class arg_exception : public std::exception {
public:
	arg_exception(std::string msg);
	virtual ~arg_exception() = default;
	virtual const char *what() const noexcept;

private:
	std::string msg;
};

/**
 * @brief class for parsing argv
 *
 * expect `argv` and `argc` as constructor parameters
 * member variables are set according to argv
 *  */
class args {
public:
	args();
	virtual ~args();
	/**
		 * parses `argv` and `argc` from main
		 *
		 * if no filter flag was present (except for -p) `print_all` is set to true
		 * in case filter flag was present, `print_all` is false and other variables
		 * are set to true only if coresponding flag was present
		 *
		 * @throws `arg_exception` if it receives invalid flags
		 *
		 *  */
		void parse(int argc, char *argv[]);

public:
	// holds true if said flag was found, false otherwise
	// in case --interface or -i flags were found but no plausible interface name
	// after it interface is set to true, but interface_name to NULL
	//
	// tcp, udp, icmp4,.... are filters for respective protocols,if no protocol is
	// specified, print_all is true and everything is printed
	bool interface = false, help = false, tcp = false, udp = false,
	     icmp4 = false, icmp6 = false, arp = false, ndp = false,
	     igmp = false, mld = false, print_all = true;
	char *interface_name;

	// nzumber, how many packets to print (-n flag)
	int num_print = 1;
	int port = -1;

private:
	/**
		 * @brief throws an exception, used when processing arguments to signalize invalid argument
		 *
		 * @param argv argv from main
		 * @param i index in argv which argument failed
		 *
		 * @throws arg_exception allways throws
		 */
	[[noreturn]] void throw_exc(char *argv[], int i);
	/**
		* @brief used to process long version of arguments (e.g. --help, --interface,...)
		*
		* @param num index to argv which argument to process next
		*
		* @return returns number of processed arguments (usually 1, 2 in case of e.g.`--interface wlp4s0`)
		* @throws arg_exception on invalid argument in argv
		* */
	int process_long(int argc, char *argv[], int num);
	/**
		* @brief used to process short version of arguments (e.g. -h, -i,...)
		*
		* @param num index to argv which argument to process next
		*
		* @return returns number of processed arguments (usually 1, 2 in case of e.g.`-i wlp4s0`)
		* @throws arg_exception on invalid argument in argv
		* */
	int process_short(int argc, char *argv[], int num);
	/**
		* @brief checks if next argument could be an interface name
		*
		* returns true if next argument doesn't begin with '-', but makes no checks
		* if it is a valid interface name.
		*
		* Returns false if next argument begins with '-' or is out of range
		*
		* @param num index to argv which was being processed (`num` + 1 is the one being checked by this function)
		*
		* @return fasle(0) if next argument is an flag (begins with '-')
		* @return true(1) if next argument _could_ be an interface
		*
		*/
	int check_int(int argc, char *argv[], int num);
	/**
	 * @brief if possible parses next argument as a port number
	 *
	 * if next argument is valid port number `port` is set, exception thrown otherwise
	 *
	 *  @throws arg_exception if port is negative or larger than 2^16
	 *  @returns number of processed argument (1)
	 */
	int check_port(int argc, char *argv[], int num);
};

#endif // ARGS_H_
