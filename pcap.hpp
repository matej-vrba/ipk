/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PCAP_H
#define PCAP_H
#include <pcap/pcap.h>
#include <string>
#include <vector>

typedef void (*pcap_callback)(class args& args, const struct pcap_pkthdr*, const u_char*);
extern pcap_t *pcap_handle; //handle to open pcap

/**
 * @brief libpcap wrapper
 *
 * constructor initializes libpcap and finds all devices.
 * destructor closes all devices and other pcap's things
 *  */
class pcap {
public:
	pcap(class args& args);
	virtual ~pcap();
	/**
	 *  @brief attempts to find `name` interface and makes the device "selected" -
	 *  this devices will be used for capturing
	 *
	 *
	 *  @param name interface to find
	 *
	 *  @return true on suecess, false if the device was not found
	 *  */
	bool select(char *name);
		/**
		* @brief prints to `stdout` all found devices
		*
		*  */
	void print();
	/**
	 * @brief completes the initialization of libpcap - opens selected device and
	 *  compiles filter.
	 *  */
	bool init();
		/**
		* @brief sets pcap filter, this needs to be called before `init`
		*  */
	void set_filter(const char *filter)
	{
		this->filter = filter;
	}
		/**
		* @breif adds `callback` to the list of all callback, callback will be called
		* in same order they were added.
		* Can be called before or after calling `init` and before or after calling `start`
		*
		*  */
	void add_callback(pcap_callback callback)
	{
		packet_listeners.push_back(callback);
	}

		/**
		 * @brief starts capturing network trafic
		 *
		 * @return returns true on success, segfault on failure
		 *  */
	bool start();

private:
	pcap_if_t *devices = NULL; // linked list of all devices
	pcap_if_t *active = NULL; // selected, active device
	const char *filter = "";
	struct bpf_program compiled_filter;
	bpf_u_int32 ip;
	class args &args;

	// vector of callbacks for pcap will send captured packets to
	std::vector<pcap_callback> packet_listeners;
};

#endif /* PCAP_H */
