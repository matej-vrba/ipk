/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LOG_H
#define LOG_H
#ifndef NO_COLORS
#define RESET "\e[0m"
#define YELLOW "\e[0;33m"
#define ORANGE "\x1b[38;2;223;95;0m"
#define RED "\x1b[38;2;223;0;95m"
#define GREEN "\x1b[38;2;135;255;0m"
#define BLUE "\x1b[38;2;53;111;228m"
#define CYAN "\x1b[38;2;95;223;255m"
#else
#define RESET ""
#define YELLOW ""
#define ORANGE ""
#define RED ""
#define GREEN ""
#define BLUE ""
#define CYAN ""
#endif

#define LOG(...)                                              \
	do {																												\
		fprintf(stderr, __VA_ARGS__);																	 \
		fprintf(stderr, "\n");																							 \
	} while (0)

#define ERR(...)																								 \
	do {																														 \
		fprintf(stderr, "Error: ");																				\
		fprintf(stderr, __VA_ARGS__);																				\
		fprintf(stderr, "\n");																							 \
	} while (0)

#endif /* LOG_H */
