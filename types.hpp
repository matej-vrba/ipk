/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TYPES_H
#define TYPES_H
#include <bits/stdint-uintn.h>

namespace ipv6
{
typedef uint32_t address_t[4];
}
namespace ipv4
{
typedef uint8_t address_t[4];
}
typedef uint8_t mac_t[6];

#endif /* TYPES_H */
