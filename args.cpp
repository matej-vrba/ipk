/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "args.hpp"
#include "log.hpp"
#include <functional>
#include <iostream>
#include <string>
#include <type_traits>
#include <map>
#include <string.h>
#include <stdlib.h>

args::args()
	: interface_name(NULL)
{
}
void args::parse(int argc, char *argv[]){
	//iterate over all arguments
	for (int i = 1; i < argc;) {
		//check that next argument starts with '-'
		if (argv[i][0] == '-') {
			// check next arg starts with "-" or "--"
			if (argv[i][1] == '-') {
				i += process_long(argc, argv, i);
			} else {
				i += process_short(argc, argv, i);
			}
		} else {
			throw_exc(argv, i);
		}
	}
}

args::~args()
{
}

void args::throw_exc(char *argv[], int i)
{
	auto str = std::string("Invlaid argument: ");
	str.append(argv[i]);
	str.append(" use --help for usage");
	throw arg_exception(str);
}

int args::process_long(int argc, char *argv[], int num)
{
	auto arg_s = std::string(argv[num]);

	std::unordered_map<std::string, bool *> map;
	map["--icmp4"] = &icmp4;
	map["--icmp6"] = &icmp6;
	map["--igmp"] = &igmp;
	map["--tcp"] = &tcp;
	map["--udp"] = &udp;
	map["--help"] = &help;
	map["--arp"] = &arp;
	map["--ndp"] = &ndp;
	map["--mld"] = &mld;

	if (map.contains(arg_s)) {
		*map[arg_s] = true;
		print_all = false;
		return 1;
	}

	if (arg_s == "--interface") {
		interface = true;
		return 1 + check_int(argc, argv, num);
	}
	if (arg_s == "--port") {
		return 1 + check_port(argc, argv, num);
	}
	throw_exc(argv, num);
}

int args::process_short(int argc, char *argv[], int num)
{
	auto arg_s = std::string(argv[num]);

	std::unordered_map<std::string, bool *> map;
	map.emplace("-t", &(this->tcp));
	map.emplace("-u", &(this->udp));
	map.emplace("-h", &(this->help));

	if (map.contains(arg_s)) {
		*map[arg_s] = true;
		print_all = false;
		return 1;
	}
	if (arg_s == "-i") {
		interface = true;
		return 1 + check_int(argc, argv, num);
	}
	if (arg_s == "-p") {
		return 1 + check_port(argc, argv, num);
	}
	if (arg_s == "-n") {
		if (num + 1 >= argc)
			throw_exc(argv, num);
		//parse next arg as base 10 number
		size_t len;
		try {
			num_print = std::stoi(argv[num + 1], &len, 10);
		} catch (const std::invalid_argument &e) {
			throw_exc(argv, num + 1);
		}
		// len stores number of processed characters
		// check that argv[num+1] contained number and nothing else
		if(len != strlen(argv[num + 1])){
			throw_exc(argv, num+1);
		}
		return 2;
	}
	throw_exc(argv, num);
}

int args::check_port(int argc, char *argv[], int num)
{
	if (num + 1 >= argc || // currently proceesed arg is last
	    argv[num + 1][0] == '-') // next argument is flag, not interface name
	{
		throw_exc(argv, num + 1);
	}
	// next argument is plausible interface name
	size_t len;
	try {
		port = std::stoi(argv[num + 1], &len, 10);
	} catch (const std::invalid_argument &e) {
		throw_exc(argv, num + 1);
	}
	if(len != strlen(argv[num + 1])){
		throw_exc(argv, num+1);
	}
	// outside valid range
	if(port >= 65536)
		throw_exc(argv, num + 1);

	return 1;
}

int args::check_int(int argc, char *argv[], int num)
{
 // if(currently proceesed arg is last || next argument is flag, not interface name)
	if (num + 1 >= argc || argv[num + 1][0] == '-')
	{
		interface_name = NULL;
		return 0;
	}
	// next argument is plausible interface name
	interface_name = argv[num + 1];
	return 1;
}

arg_exception::arg_exception(std::string msg)
	: msg(msg)
{
}
const char *arg_exception::what() const noexcept
{
	return msg.c_str();
}
