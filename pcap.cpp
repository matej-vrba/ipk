/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "pcap.hpp"
#include <iostream>
#include "log.hpp"
#include <pcap/pcap.h>
#include <sys/types.h>
#include "args.hpp"
#include <cstring>

pcap_t *pcap_handle = NULL;


#define CAPTURE_MAX 2048

pcap::pcap(class args &args)
	: args(args)
{
	char errbuff[PCAP_ERRBUF_SIZE];

	pcap_if_t *dev = NULL;

	int errc = pcap_findalldevs(&dev, errbuff);
	if (errc) {
		ERR("Couldn't get list of aviable devices: %s", errbuff);
		throw "PCAP error";
	}

	if (pcap_init(PCAP_CHAR_ENC_UTF_8, errbuff)) {
		ERR("Failed to initialize PCAP: %s", errbuff);
		throw "PCAP error";
	}
	devices = dev;
}
bool pcap::init()
{
	char errbuff[PCAP_ERRBUF_SIZE];

	pcap_handle =
		pcap_open_live(active->name, CAPTURE_MAX, true, 1024, errbuff);
	if (pcap_handle == NULL) {
		ERR("Failed to open %s for reading: %s", active->name, errbuff);
		return false;
	}
	if (pcap_datalink(pcap_handle) != DLT_EN10MB) {
		ERR("Device %s not supported: %s", active->name, errbuff);
		return false;
	}

	//compile filter
	if (pcap_compile(pcap_handle, &compiled_filter, filter, 0, ip) == -1) {
		ERR("Failed to parse filter %s, error: %s", filter,
		    pcap_geterr(pcap_handle));
		return false;
	}
	//use filter
	if (pcap_setfilter(pcap_handle, &compiled_filter) == -1) {
		ERR("Failed to use filter %s, error: %s", filter,
		    pcap_geterr(pcap_handle));
		return false;
	}
	pcap_freecode(&compiled_filter);
	return true;
}
pcap::~pcap()
{
	if (devices)
		pcap_freealldevs(devices);
	devices = NULL;
	pcap_freecode(&this->compiled_filter);

	if (pcap_handle)
		pcap_close(pcap_handle);
}

bool pcap::select(char *name)
{
	pcap_if_t *tmp = devices;
	while (tmp) {
		if (strcmp(tmp->name, name) == 0) {
			active = tmp;
			return true;
		}
		tmp = tmp->next;
	}
	return false;
}

void pcap::print()
{
	pcap_if_t *tmp = devices;
	printf("List of aviable devices:\n");
	while (tmp) {
		printf("%s\n", tmp->name);
		tmp = tmp->next;
	}
}
bool pcap::start()
{
	pcap_loop(
		pcap_handle, 0,
		// lambda that goes through all callbacks and calls them
		// using the char_u* user pointer to pass a pointer to this function to the
		// lambda
		// this pointer is not passed to callbacks
		[](u_char *args, const struct pcap_pkthdr *header,
		   const u_char *packet) {
			pcap* pcap_inst = ((pcap *)args); // pointer to the instance of pcap class


			for (auto &callback : pcap_inst->packet_listeners)
				callback(pcap_inst->args, header, packet);
			if(pcap_inst->args.num_print == 0)
				pcap_breakloop(pcap_handle);
		},
		(u_char *)this);
	return true;
}
