# Changelog

Parsing of all packets from assignment should work.


Limitation:
- Output colors can only be disables using `NO_COLORS` during compilation
- Not all IGMP and MLD messages were properly tested and so some outputs can be wrong.
- No parsing of TCP flags
- This project might not compile on windows and definitely will not compile using msvc
