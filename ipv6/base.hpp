/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef IPV6_BASE_H
#define IPV6_BASE_H
#include "../frame.hpp"
#include "icmp.hpp"
#include <bits/stdint-uintn.h>

#include "../types.hpp"

namespace ipv6
{

enum class next_header {
	ICMP = 58,
	HOP_BY_HOP = 0,
};

void read_ipv6(uint32_t *dest, const u_char *data);
void v6toh(uint32_t *addr);

	//https://www.rfc-editor.org/rfc/rfc2460#section-4.3
struct ipv6packet : public ::packet {
public:

	struct {
		#pragma pack(push, 1)
		uint32_t version_class_flow_label;
		uint16_t payload_len;
		uint8_t next_header;
		uint8_t hop_limit;
		address_t src_add;
		address_t dst_add;
		#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	ipv6packet(const struct pcap_pkthdr *header, const u_char *packet,
		   size_t used);
	virtual void print() override;
	virtual ~ipv6packet();
	virtual bool should_print(class args &args) override;

private:
	packet *next = NULL;
};

}

#endif /* IPV6_BASE_H */
