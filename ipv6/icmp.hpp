/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ICMPV6_H
#define ICMPV6_H
#include "../frame.hpp"
#include "base.hpp"
#include <bits/stdint-uintn.h>
#include "../types.hpp"

namespace ipv6
{

enum class icmp_type {
	RS = 133,
	RA = 134,
	NS = 135,
	NA = 136,
	Redirect = 137,
	MLDReportv1 = 131,
	MLDDonev1 = 132,
	MLDv2 = 143,
};

struct ndp_packet;
struct icmp_packet : public ::packet {
public:
	// icmpv6 packet fields

	struct {
		#pragma pack(push, 1)
		uint8_t type;
		uint8_t code;
		uint16_t checksum;
		#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	icmp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		    size_t used);
	virtual void print() override;
	virtual bool should_print(class args &args)override;

		~icmp_packet();
protected:
	icmp_packet()
	{
	}
		packet *next = NULL;
};



}

#endif /* ICMPV6_H */
