/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "icmp.hpp"
#include <bits/stdint-uintn.h>
#include <cstdlib>
#include "ndp.hpp"
#include "mld.hpp"
#include "../args.hpp"
#include <cstring>

namespace ipv6
{
icmp_packet::icmp_packet(const struct pcap_pkthdr *header, const u_char *packet,
			 size_t used)
{
	//parse ICMPv6
	memcpy(&data, packet + used, sizeof(data));


	// check if icmp is ndp
	size_t len = sizeof(data);
	switch (data.type) {
	case (int)icmp_type::NS: {
		next = new ndp_ns_packet (header, packet, used + len);
		break;
	}
	case (int)icmp_type::NA: {
		next = new ndp_na_packet(header, packet, used + len);
		break;
	}
	case (int)icmp_type::RS: {
		next = new ndp_rs_packet(header, packet, used + len);
		break;
	}
	case (int)icmp_type::RA: {
		next =new ndp_ra_packet(header, packet, used + len);
		break;
	}
	case (int)icmp_type::MLDv2: {
	case (int)icmp_type::MLDReportv1:
	case (int)icmp_type::MLDDonev1:
		// -1 to include type
		next = new mld_packet(header, packet, used);
		break;
	}
	}
}

void icmp_packet::print()
{
	printf("> %sICMP%s\n", BLUE, RESET);
	if(data.type == 128)
		printf("type: request\n");
	else if (data.type == 129)
		printf("type: reply\n");
	else
		DATA_PRINT(type);
	if(next)
		next->print();
}

	icmp_packet::~icmp_packet(){
		if(next)
			delete next;

	}
bool icmp_packet::should_print(class args &args)
{
	if(next)
		return args.print_all || args.icmp6 || next->should_print(args);

	return args.print_all || args.icmp6;
}

}
