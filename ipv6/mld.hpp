/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MLD_H
#define MLD_H
#include "icmp.hpp"
#include "../types.hpp"

namespace ipv6
{

#pragma pack(push, 1)
struct mld_record_t {
	uint8_t record_type;
	uint8_t aux_data_len;
	uint16_t num_sources;

	address_t multicast_addr;
	address_t *src_addrs;
	uint32_t *aux_data;
};
#pragma pack(pop)

// MLDv2 packet
struct mld_packet : public icmp_packet {
public:
#pragma pack(push, 1)
	union {
		struct {
			uint8_t code;
			uint16_t checksum;
			uint16_t max_resp_delay;
			uint16_t reserved;
			address_t multicast_addr;
		} v1;
		struct {
			uint8_t code;
			uint16_t checksum;
			uint16_t reserved;
			uint16_t address_record_num;
		} v2;
	} data;
#pragma pack(pop)
		uint8_t type = 0;
	struct mld_record_t *records;

public:
	virtual void print() override;

		/** @see packet::packet */
	mld_packet(const struct pcap_pkthdr *header, const u_char *packet,
		   size_t used);
	virtual bool should_print(class args &args) override;
};

}
#endif /* MLD_H */
