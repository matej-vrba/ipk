/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NDP_H
#define NDP_H
#include "icmp.hpp"
#include "../types.hpp"

namespace ipv6
{
// https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol
// https://www.rfc-editor.org/rfc/rfc4861
//
// base NDP packet, NS, NA, RS and RA inherit from this class
/**
 * Base class for all ndp packets, should not be constructed directly
 *
 *  */
struct ndp_packet : public icmp_packet {
public:
	virtual void print() override
	{
	}

	virtual bool should_print(class args &args) override;

protected:
	ndp_packet()
	{
	}
};

	/**
	* Class representing neighbor solicitation message
	*  */
struct ndp_ns_packet : public ndp_packet {
public:

	struct {
		#pragma pack(push, 1)
		uint32_t reserved;
		address_t target_add;
		#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	ndp_ns_packet(const struct pcap_pkthdr *header, const u_char *packet,
		      size_t used);
	virtual void print() override;
};

// neighbor advertisement message
struct ndp_na_packet : public ndp_packet {
public:

	struct {
		#pragma pack(push, 1)
		uint32_t flags;
		address_t target_add;
		#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	ndp_na_packet(const struct pcap_pkthdr *header, const u_char *packet,
		      size_t used);
	virtual void print() override;
};

// router solicitation message
struct ndp_rs_packet : public ndp_packet {
public:

	struct {
		uint32_t reserved;
	} data;


public:
		/** @see packet::packet */
	ndp_rs_packet(const struct pcap_pkthdr *header, const u_char *packet,
		      size_t used);
	virtual void print() override;
};

// router advertisement message
struct ndp_ra_packet : public ndp_packet {
public:

	struct {
		#pragma pack(push, 1)
		uint8_t hop_limit;
		uint8_t flags;
		uint16_t router_lifetime;
		uint32_t reachable_time;
		uint32_t retrans_time;
		#pragma pack(pop)
	} data;


public:
		/** @see packet::packet */
	ndp_ra_packet(const struct pcap_pkthdr *header, const u_char *packet,
		      size_t used);
	virtual void print() override;
};

}

#endif /* NDP_H */
