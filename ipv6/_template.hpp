/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef IPV6_BASE_H
#define IPV6_BASE_H
#include "../pbuilder.hpp"

namespace ipv6
{
struct template_packet : public virt_packet {
public:

	struct {
	} data;


public:
	template_packet(const struct pcap_pkthdr *header, const u_char *packet);
	virtual void print();
};

}

#endif /* IPV4_BASE_H */
