/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "ndp.hpp"
#include "../args.hpp"
#include <cstring>

namespace ipv6
{
ndp_ns_packet::ndp_ns_packet(const struct pcap_pkthdr *header,
			     const u_char *packet, size_t used)
{
	//read v6 address
	memcpy(&data, packet + used, sizeof(data));
	v6toh(data.target_add);
}
void ndp_ns_packet::print()
{
	printf("> %sNDP NS%s\n", BLUE, RESET);
	print_ipv6("NS target add: ", data.target_add);
}

bool ndp_packet::should_print(class args &args)
{
	return args.print_all || args.ndp;
}

ndp_na_packet::ndp_na_packet(const struct pcap_pkthdr *header,
			     const u_char *packet, size_t used)
{
	memcpy(&data, packet + used, sizeof(data));
	data.flags = ntohl(data.flags);
	v6toh(data.target_add);
}
void ndp_na_packet::print()
{
	printf("> %sNDP NA%s\n", BLUE, RESET);
	printf("r flag: %d\n", (data.flags >> 31) & 1);
	printf("s flag: %d\n", (data.flags >> 30) & 1);
	printf("o flag: %d\n", (data.flags >> 29) & 1);
	print_ipv6("NA Target address: ", data.target_add);
}

ndp_rs_packet::ndp_rs_packet(const struct pcap_pkthdr *header,
			     const u_char *packet, size_t used)
{
}
void ndp_rs_packet::print()
{
	printf("> %sNDP RS%s\n", BLUE, RESET);
}
ndp_ra_packet::ndp_ra_packet(const struct pcap_pkthdr *header,
			     const u_char *packet, size_t used)
{
	memcpy(&data, packet + used, sizeof(data));
	data.router_lifetime = ntohs(data.router_lifetime);
	data.reachable_time = ntohl(data.reachable_time);
	data.retrans_time = ntohl(data.retrans_time);
}
void ndp_ra_packet::print()
{
	printf("> %sNDP RA%s\n", BLUE, RESET);
	DATA_PRINT(hop_limit);
	printf("m flag: %d\n", (data.flags & 1) != 0);
	printf("o flag: %d\n", (data.flags & 2) != 0);
	DATA_PRINT(router_lifetime);
}

}
