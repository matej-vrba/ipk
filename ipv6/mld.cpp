/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "mld.hpp"
#include "../args.hpp"
#include "base.hpp"
#include "../types.hpp"
#include <cstring>

#define MLD_REPORT 131
#define MLD_DONE 132
#define MLDv2 143

namespace ipv6
{

void mld_packet::print()
{
	if (type == MLDv2) {
		const char *map[] = {
			"Current state is INCLUDE",
			"Current state is EXCLUDE",
			"State change to INCLUDE",
			"State change to EXCLUDE",
			"Source change to ALLOW NEW",
			"Source change to BLOCK OLD",
		};

		printf("> %sMLDv2%s\n", BLUE, RESET);
		for (int i = 0; i < data.v2.address_record_num; i++) {
			if (records[i].record_type < 6)
				printf("Record type: %s\n",
				       map[records[i].record_type - 1]);
			else
				printf("Unknows record type\n");

			print_ipv6("multicast: ", records[i].multicast_addr);
			for (int j = 0; j < records[i].num_sources; j++)
				print_ipv6("address: ",
					   records[i].src_addrs[j]);
		}
	} else if (type == MLD_REPORT || type == MLD_DONE) {
		printf("> %sMLDv1%s\n", BLUE, RESET);
		print_ipv6("address: ", data.v1.multicast_addr);
	}
}

bool mld_packet::should_print(class args &args)
{
	return args.print_all || args.mld;
}
mld_packet::mld_packet(const struct pcap_pkthdr *header, const u_char *packet,
		       size_t used)
{
	type = *(packet + used);
	if (type == MLDv2) {
		//parse the beginning of the mld packet
		auto next_record = packet + used + 1;
		size_t len = sizeof(data.v2);
		memcpy(&data.v2, next_record, len);
		next_record += len;
		data.v2.address_record_num = ntohs(data.v2.address_record_num);
		data.v2.checksum = ntohs(data.v2.checksum);
		//allocate memory for all records
		records = new struct mld_record_t[data.v2.address_record_num]();

		//set pointer to first record
		// parse all records
		for (int i = 0; i < data.v2.address_record_num; i++) {
			//size of one record without src_addrs and aux_data fields
			// size of void* is there to get size of src_addrs without warning
			size_t base_size =
				sizeof(mld_record_t) - sizeof(void *) * 2;
			memcpy(records + i, next_record, base_size);
			next_record += base_size;
			v6toh(records[i].multicast_addr);

			// allocate memory for addresses
			records[i].src_addrs = (address_t *)malloc(
				sizeof(address_t) * records[i].num_sources);

			//move next_record to point to first address
			for (int j = 0; j < records[i].num_sources; j++) {
				//load address
				read_ipv6(records[i].src_addrs[j], next_record);
				// move next_record to point to another address
				next_record += sizeof(address_t);
			}

			// skip aux data, next_record should point to next mld record
			next_record += records[i].aux_data_len;
		}
	} else if (type == MLD_REPORT || type == MLD_DONE) {
		memcpy(&data.v1, packet + used + 1, sizeof(data.v1));
		v6toh(data.v1.multicast_addr);
	}
}
}
