/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "base.hpp"
#include "icmp.hpp"
#include <assert.h>
#include <cstring>
#include <bits/stdint-uintn.h>

#include "../args.hpp"

namespace ipv6
{

void read_ipv6(uint32_t *dest, const u_char *data)
{
	memcpy(dest, data, 16);
	for (int i = 0; i < 4; i++)
		dest[i] = ntohl(dest[i]);
}

void v6toh(uint32_t *addr)
{
	for (int i = 0; i < 4; i++)
		addr[i] = ntohl(addr[i]);
}

ipv6packet::ipv6packet(const struct pcap_pkthdr *header, const u_char *packet,
		       size_t used)
{
	memcpy(&data, packet + used, sizeof(data));
	data.version_class_flow_label = ntohl(data.version_class_flow_label);
	data.payload_len = ntohs(data.payload_len);
	for (int i = 0; i < 4; i++)
		data.src_add[i] = ntohl(data.src_add[i]);
	for (int i = 0; i < 4; i++)
		data.dst_add[i] = ntohl(data.dst_add[i]);

	int len = sizeof(data);
	len = 40;
	switch (data.next_header) {
	case (int)next_header::ICMP: {
		next = new icmp_packet(header, packet, used + len);
		return;
	}
	case (int)next_header::HOP_BY_HOP: {
		uint8_t next_header = *(packet + used + sizeof(data));
		if (next_header ==
		    (int)next_header::ICMP) { // next_header == ICMPv6

			// parsing single field(len) from hop by hop header
			// get the beginning of hop by hop header
			const uint8_t *hbh_header =
				packet + used + sizeof(data);
			// get the length (second byte)
			uint8_t hbh_len = hbh_header[1];
			// length is number of bytes * 8 - 1
			hbh_len += 1;

			next = new icmp_packet(header, packet,
					       used + len + hbh_len * 8);
			return;
		}
	}
	}
}
void ipv6packet::print()
{
	printf("> %sIPv6 Header%s\n", ORANGE, RESET);
	print_ipv6("src IP: ", data.src_add);
	print_ipv6("dst IP: ", data.dst_add);
	DATA_PRINT(hop_limit);
	if (next)
		next->print();
}
bool ipv6packet::should_print(class args &args)
{
	if (next)
		return next->should_print(args);
	else
		return args.print_all;
}
ipv6packet::~ipv6packet()
{
	if (next)
		delete next;
}
}
