/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "frame.hpp"
#include <bits/stdint-uintn.h>
#include <cstddef>
#include <cstring>
#include "ipv4/ipv4.hpp"
#include "ipv6/ipv6.hpp"
#include <assert.h>
#include <time.h>
#include <chrono>
#include "args.hpp"
#include <math.h>

void print_mac(uint8_t *mac)
{
	for (int i = 0; i < 6; i++) {
		printf("%02x", mac[i]);
		if (i != 5)
			printf(":");
	}
	printf("\n");
}

void print_mac(const char* msg, uint8_t *mac)
{
	printf("%s", msg);
	print_mac(mac);
}

//prints input data specified in `packet`, with length from `len`
void hex_printer(const u_char *packet, size_t len)
{
	size_t len_div16 = ceil(len / 16.0);

	for (size_t i = 0; i < len_div16; i++) {
		printf("0x%03lx0: ", i); // "hex line number"
		// the format is:
		// `0x` - print "0x"
		// `%03lx` - print i in hex format and make it 3 chars (aka print 1 as 001)
		// `0: ` - trailing 0, could be removed and `i` replaced with `i*16`

		// hexadecimal representation
		for (int j = 0; j < 16; j++) {
			if (j == 7)
				printf(" "); //< 8 byte separation
			if (i * 16 + j < len)
				printf("%02x ",
				       packet[i * 16 + j]); //< print one byte
			else
				printf("00 "); //< print padding if we're past `len`
		}
		// ascii representation
		for (int j = 0; j < 16; j++) {
			// check if we're not past the end of data and current byte is pritable
			if (i * 16 + j < len && isprint(packet[i * 16 + j]))
				printf("%c", packet[i * 16 + j]);
			else
				printf("."); //< current byte is not printable, print '.' instead
			if (j == 7)
				printf(" "); //< 8 byte separation
		}
		printf("\n");
	}
}

void print_ipv4(const char *msg, uint8_t addr[])
{
	printf("%s%d.%d.%d.%d\n", msg, addr[0], addr[1], addr[2], addr[3]);
}

void print_ipv6(const char *msg, uint32_t *addr)
{
	printf("%s", msg);
	// true if in previous iteration was `addr[...]...` zero
	// used to prit "::"
	// `printed_shorted` - true if shorted version was aleready printed, to avoid
	// priting something like fe80::a::1
	bool was_zero = false, printed_shorted = false;
	for (int i = 0; i < 8; i++) {
		uint16_t part;
		//copy correct part of address to `part` buffer
		if (i % 2) {
			part = addr[i / 2] & 0xffff;
		} else {
			part = (addr[i / 2] >> 16) & 0xffff;
		}

		//check if "::" should be printed
		if (part == 0 && was_zero == false && !printed_shorted) {
			printf(":");
		}
		//if current part is not zero print it
		if (part != 0 || printed_shorted) {
			if (was_zero) printed_shorted = true;
			printf("%x", part);
			if (i != 7) // if this is not last part, print ":"
				printf(":");
		} else {
			// save info that in current iteration printed "::"
			// respectively previous iteration printed "xx:" nad this printed only ":"
			was_zero = true;
			if (i == 0)
				printf(":");
		}
	}
	printf("\n");
}

void build_packaet(class args& args, const struct pcap_pkthdr *header, const u_char *packet)
{
	frame f(header, packet);
	f.print(args);
	//printf("\n");
}

frame::frame(const struct pcap_pkthdr *header, const u_char *packet)
{
	// we can parse ethernet header using memcpy
	size_t ether_size = sizeof(data);
	memcpy(&this->data, packet, ether_size);
	//fix multibyte part
	this->data.type = htons(this->data.type);
	this->len = header->len;
	this->time = header->ts;
	this->header = header;
	this->packet = packet;

	switch (this->data.type) {
	case (uint16_t)frame_type::ARP: {
		this->next = new ipv4::arp_packet(header, packet, ether_size);
		return;
	}
	case (uint16_t)frame_type::IPv4: {
		this->next = new ipv4::ipv4packet(header, packet, ether_size);
		return;
	}
	case (uint16_t)frame_type::IPv6: {
		this->next = new ipv6::ipv6packet(header, packet, ether_size);
		return;
	}
	}
}

void packet::print()
{
	assert(false); //inheriting classes should implement
}

void frame::print(class args& args)
{
	if(!next) return;
	if(!next->should_print(args))return;
	struct tm *date = gmtime(&time.tv_sec);
	if(args.num_print >= 0)
		args.num_print--;

	char formatted_date[40];
	strftime(formatted_date, 40, "%Y-%m-%dT%T", date);
	printf("> %sEthernet frame%s\n", RED, RESET);
	printf("timestamp: %s.%ldZ\n", formatted_date, time.tv_usec / 1000);
	printf("src mac: ");
	print_mac(data.dst);
	printf("dst mac: ");
	print_mac(data.src);
	printf("frame len: %d Bytes\n", len);
	if (next)
		next->print();
	hex_printer(packet, header->len);
	printf("\n");
}
frame::~frame()
{
	if (next)
		delete next;
}

bool packet::should_print(class args &args){
	return false;
}
