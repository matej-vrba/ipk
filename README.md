# Project 2 - ZETA: Network sniffer
IPK sniffer is a command line utility allowing capturing and basic decoding of
ethernet frames and their content.

## Compiling
To compile the projct use included Makefile.
You will need `libm` and `libpcap` libraries.

On systems that do not provide dev package for `libpcap` (often called
`libpcap-dev`) put header files into `include` directory and compiled library
into `lib` directory.

Project can be compiled using `make -e RELEASE=true`

## Usage
Sniffer can be launched either as root or with `CAP_NET_RAW` capability set like
this:

``` sh
./ipk-sniffer [OPTIONS]
```

### Possible options
#### Interface
Interface can be specified using `-i` or `--interface` flag followed by name of
interface on which to start capturing traffic.

If interface flag is missing or is not followed by interface name, list of
network interfaces is printed.

#### Transport protocols
IPK-Sniffer can filter TCP (`-t` or `--tcp` flags) and UDP (`-u` or `--udp` flags)
traffic.

Port flag (`-p` or `--port`) can be used to filter UDP and TCP traffic based on
source and destination port.
Using port flag in combination with any other filtering flag has no effect.

#### Other filtering flags
IPK-Sniffer also supports filters for:
- ARP - `--arp`
- IGMP - `--igmp`
- IPv4 ICMP - `--icmp4`
- IPv6 ICMP - `--icmp6`
- NDP - `--ndp`
- MLD - `--mld`

Multiple flags can be mixed to get "one of" - first packet matching one of the
specified flags.

#### Number flag
`-n` flag can be used followed by number of packets to print.
If number flag is not specified only one packed is printed.

Specifying negative number (or 0) will cause the sniffer to continue printing
until it is terminated using `C-c` or an error occurs.

### Colors
Unless `NO_COLORS` is defined (by adding `-DNO_COLORS` to c++ flags in makefile)
output is colored.
The colors should loosely corespond to layers of ISO-OSI model, but should be
only be used to make navigation easier.

## TESTING
For testing I've decided to create packets using simulated router in GNS3,
capture and filter said packets using wireshark and replay using `tcpreplay`.

But for TCP and UDP data all I had to do is listen on my computer's network
interface and every time I got all I needed.

Files used for testing are included in "pcaps" directory and can be used in
`tcpreplay`. In file TESTS.md are outputs of IPK-Sniffer(1st) and wireshard(2nd).
These tests should include most packets that are parsed by the sniffer
(including some TCP and UDP).

To reproduce the test results, open wireshark listening on loopback and two
terminal windows in project's root directory, and run
- `sudo ./ipk-sniffer -i lo -n 26`
- `sudo tcpreplay -t -i lo pcaps/*.pcapng`
this assumes that on your machine there exists loopback interface named `lo`.


## Structure

![uml of packets](uml.png)

Project contains `packet` base class and one class for (more or less) each
header it is able to parse.

When parsing packets, every header that is not final (there can be another
packet following it e.g. tcp header inside IPv4, ...) it contains pointer to
next header, something like this:

![relationship between packet instances](uml2.png)

Note that in this case the relationship in this UML is not "derives from", but
"has a reference to".
This should help with adding support for more packet types in future.

### Support structures
Parsing of arguments is done using `args` module.
This module contains class with same name, with `parse` method that takes `argv`
and `argc` and either successfully parses them or throws an exception.

This class includes public member variable for each argument.

`log` module included definition of colors and macros for easier printing.
`types` module contains typedef for IPv4, IPv6 and MAC addresses.

`pcap` module is a wrapper for `libpcap` allowing basic object oriented
interaction with this library.
Interface of this module is described in `pcap.hpp` and can be generated into
doxygen documentation using `doxygen` or `make docs` commands.

## Theory necessary to understand the functionality of the implementation

Using `libpcap` packets are captures, (not only) Ethernet header have a `type`
field containing information about next header (if any exists).
Using this information and structure of the expected header, it is ususally
possible to create c++ struct with same fields in same order.

By doing this and disabling padding it is possible to parse the packet by
copying the received data directly into instance of this struct and using
ntohs/ntohl to correct 16 and 32 bit fields (convert them from big endian to
little endian).


## Bibliography

- [C++ reference](https://en.cppreference.com/w/)
- [libpcap](https://www.tcpdump.org/)
- [libpcap usage](https://www.devdungeon.com/content/using-libpcap-c)
- [EtherType](https://en.wikipedia.org/wiki/EtherType)
- [Neighbor Discovery Protocol](https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol)
- [Address_Resolution_Protocol](https://en.wikipedia.org/wiki/Address_Resolution_Protocol)
- [Internet_Group_Management_Protocol](https://en.wikipedia.org/wiki/Internet_Group_Management_Protocol)
- [Internet_Control_Message_Protocol](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol)
- [IPv6](https://en.wikipedia.org/wiki/IPv6)
- [ICMPv6](https://en.wikipedia.org/wiki/ICMPv6)
- [Transmission_Control_Protocol](https://en.wikipedia.org/wiki/Transmission_Control_Protocol)
- [User_Datagram_Protocol](https://en.wikipedia.org/wiki/User_Datagram_Protocol)
- [Cisco - igmp information and configuration](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/ipmulti_igmp/configuration/xe-16/imc-igmp-xe-16-book/imc-customizing-igmp.html)
- [Cisco - mld configuration](https://www.cisco.com/c/en/us/td/docs/routers/ios/config/17-x/ip-multicast/b-ip-multicast/m_ip6-mcast-mld-limits.html)
- [RFC 791](https://www.rfc-editor.org/rfc/rfc791)
- [RFC 792](https://www.rfc-editor.org/rfc/rfc792)
- [RFC 793](https://www.rfc-editor.org/rfc/rfc793)
- [RFC 826](https://www.rfc-editor.org/rfc/rfc826)
- [RFC 2236](https://www.rfc-editor.org/rfc/rfc2236)
- [RFC 2236](https://www.rfc-editor.org/rfc/rfc2236)
- [RFC 2460](https://www.rfc-editor.org/rfc/rfc2460)
- [RFC 2463](https://www.rfc-editor.org/rfc/rfc2463)
- [RFC 2710](https://www.rfc-editor.org/rfc/rfc2710)
- [RFC 3376](https://www.rfc-editor.org/rfc/rfc3376)
- [RFC 3376](https://www.rfc-editor.org/rfc/rfc3376)
- [RFC 3810](https://www.rfc-editor.org/rfc/rfc3810)
- [RFC 4443](https://www.rfc-editor.org/rfc/rfc4443)
- [RFC 4861](https://www.rfc-editor.org/rfc/rfc4861)
- [RFC 4861](https://www.rfc-editor.org/rfc/rfc4861)
- [RFC 8200](https://www.rfc-editor.org/rfc/rfc8200)
