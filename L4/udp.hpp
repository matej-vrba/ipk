/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef UDP_H
#define UDP_H
#include "../frame.hpp"

namespace l4
{

	/**
	* @brief representation of udp packet
	*
	* Used to parse udp packets, contain header information in `data` struct
	* pointer to packet body is in `packet_data` with body length in `packet_data_len`
	*
	*
	*/
class udp_packet : public ::packet {
public:

	struct {
		#pragma pack(push, 1)
		uint16_t src_port;
		uint16_t dst_port;
		uint16_t len;
		uint16_t checksum;
		#pragma pack(pop)
	} data;

	const uint8_t *packet_data = NULL;
	size_t packet_data_len;
public:
		/** @see packet::packet */
	udp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		   size_t used);
		/**
		 *  If `args` contains info to print udp or everything, source and destination
		 *  port, and packets body is printed to stdout.
		 *  Packet body is printed using `hex_printer` function
		 *
		 *  @param args initialized args class
		 *  */
	virtual void print() override;
	virtual bool should_print(class args &args) override;
	virtual ~udp_packet()
	{
	}
};
}

#endif /* UDP_H */
