/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TCP_H
#define TCP_H
#include "../frame.hpp"
#include <bits/stdint-uintn.h>

namespace l4
{
	/**
	* @brief Class representation of TCP packet
	*
	* contains all header fields in `data` struct.
	* `options` contains pointer past the end of the heder, where options should be
	* if present, null pointer otherwise
	*
	*  */
class tcp_packet : public ::packet {
public:

	struct {
		#pragma pack(push, 1)
		uint16_t src_port;
		uint16_t dst_port;
		uint32_t sequence_n;
		uint32_t ack_n;
		uint8_t data_off_reserved;
		uint8_t flags;
		uint16_t window;
		uint16_t checksum;
		uint16_t urgent_pointer;
		#pragma pack(pop)
	} data;

	const uint8_t *options = NULL;
	const uint8_t *packet_data = NULL;
	uint32_t packet_data_len;

public:
		/** @see packet::packet */
	tcp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		   size_t used);
		/**
		 * Prints info from tcp packet if instructed by `args`
		 *
		 * @param args args struct initialized from command line args
		 */
	virtual void print() override;
	virtual bool should_print(class args &args) override;
};
}

#endif /* TCP_H */
