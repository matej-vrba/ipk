/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "tcp.hpp"
#include <cstring>
#include "../args.hpp"

namespace l4
{
tcp_packet::tcp_packet(const struct pcap_pkthdr *header, const u_char *packet,
		       size_t used)
{
	memcpy(&data, packet + used, sizeof(data));
	data.src_port = ntohs(data.src_port);
	data.dst_port = ntohs(data.dst_port);
	data.sequence_n = ntohl(data.sequence_n);
	data.ack_n = ntohl(data.ack_n);
	data.window = ntohs(data.window);
	data.checksum = ntohs(data.checksum);
	data.urgent_pointer = ntohs(data.urgent_pointer);


	// set reserved bits to 0
	data.data_off_reserved &= 0xf;


	if(data.data_off_reserved > 5)
		options = packet + sizeof(data);
	else
		options = NULL;

	packet_data = packet + used + data.data_off_reserved;
	packet_data_len = header->len - used - data.data_off_reserved;

	//TODO: options
}
void tcp_packet::print()
{
	printf("> %sTCP header%s\n", BLUE, RESET);
	DATA_PRINT(src_port);
	DATA_PRINT(dst_port);
	printf("> %sTCP data%s\n", CYAN, RESET);
}

	bool tcp_packet::should_print(class args &args) {
		if(args.port >= 0)
			return (args.print_all || args.tcp) && (args.port == data.dst_port || args.port == data.src_port);
		return args.print_all || args.tcp;
	}

}
