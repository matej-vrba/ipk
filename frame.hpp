/*
    Copyright (C) 2023  Matěj Vrba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef FRAME_H
#define FRAME_H
#include <bits/stdint-uintn.h>
#include <pcap/pcap.h>
#include <sys/types.h>
#include "log.hpp"
#include "types.hpp"
#include <assert.h>

// prints packed in hexadecimal format
void hex_printer(const u_char *packet, size_t len);
// prints mac address in usual format (xx:xx:xx:xx:xx:xx\n)
void print_mac(uint8_t *mac);
void print_mac(const char *msg, uint8_t *mac);
// prints ipv4 in usual format (xxx.xxx.xxx.xxx) with `msg` before it
void print_ipv4(const char *msg, uint8_t addr[]);
// prints ipv6 in non-standard format (includes all 0s)
void print_ipv6(const char *msg, uint32_t *addr);

template <typename T> void print_bin(T bin)
{
	printf("0b");
	for (int i = 0; i < sizeof(T) * 8; i++) {
		if (bin & (1 << (sizeof(T) * 8 - 1)))
			printf("1");
		else
			printf("0");
		bin <<= 1;
	}
	printf("\n");
}

#define DATA_PRINT(dat) printf("%s: %d\n", #dat, data.dat)

//function that will attempt to reconstruct a packet
void build_packaet(class args &args, const struct pcap_pkthdr *header,
		   const u_char *packet);

// Possible packet types, with their type as value
// the value can be found for example here
// https://en.wikipedia.org/wiki/EtherType
enum class frame_type {
	ARP = 0x0806,
	IPv4 = 0x0800,
	IPv6 = 0x86dd,
};

class frame;
namespace ipv6
{
struct icmp_packet;
}

// virtual struct all packets should inherit from
struct packet {
	friend frame;
	friend ipv6::icmp_packet;
	/**
	 *  Prints relevant information about this header and calls print of next
	 *  any header if exists
	 *
	 *
	 *  */
	virtual void print();
	/**
	 *  Returns true when packed should be printed.
	 *
	 *  This is used to ask next header if it should be printed (ip addresses
	 *  should be printed before tcp content -> IP header calls `should_print`
	 *  of next header if it returns true it prints itself and calls print of
	 *  next header)
	 *
	 *  @param args args class, used to decide whether to print or not
	 *  @returns true if it should be printed, false if not
	 *  */
	virtual bool should_print(class args &args);
	virtual ~packet()
	{
	}

	/**
		 * Initializes coresponding packet
		 *
		 * @param header header struct returned by pcap
		 * @param paket packet data *as returned* by pcap
		 * @param used offset from the beginning of packet where tcp packet should begin
		 *  */
	packet()
	{
	}

protected:
};

//single ethernet frame
class frame : public packet {
public:
	struct {
		mac_t dst;
		mac_t src;
		//packet type https://en.wikipedia.org/wiki/EtherType
		uint16_t type;
	} data;
	uint32_t len;
	struct timeval time;

public:
		/** @see packet::packet */
	frame(const struct pcap_pkthdr *header, const u_char *packet);
	void print(class args& args);
	virtual ~frame();

private:
	packet *next = NULL; // ipv4 or ipv6 header
	const struct pcap_pkthdr *header = NULL;
	const u_char *packet = NULL;
};

#endif /* FRAMEH */
